﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileTourelle : MonoBehaviour
{
    public Transform target;
    public float speed;

    public LayerMask targetLayer;

    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Tourelle").GetComponent<TourelleSphere>().shootTarget; //récupère la cible de la tourelle
    }

    
    void Update()
    {
        
        transform.LookAt(target);                                                    //  
        transform.Translate(Vector3.forward * speed * Time.deltaTime);    //
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(((1<<other.gameObject.layer) & targetLayer) != 0)
        {
            //other.TakeDamage(10f);
            Destroy(gameObject);
        }
    }
}
