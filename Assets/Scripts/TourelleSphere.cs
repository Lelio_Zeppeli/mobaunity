﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Schema;
using UnityEngine;

public class TourelleSphere : MonoBehaviour
{
    public GameObject fire;
    
    public Transform firepoint;
    public Transform shootTarget;
    
    public List<Transform> actualTargets = new List<Transform>();
    
    public int nbTargets = 0;
    public LayerMask targetLayer;

    public float lifePoints;
    public bool ravaged;

    public float fireRate;
    private float _cooldown;
    
    
    
    
    void Awake()
    {
        _cooldown = fireRate;
    }

    
    
   
    void Update()
    {
        if (nbTargets > 0)
        {
            Debug.DrawLine(firepoint.position,actualTargets[0].position);
            shootTarget = actualTargets[0];
            fireRate -= Time.deltaTime;
            if (fireRate <= 0f)
            {
                Shoot();
                fireRate = _cooldown;
            }
        }
        else
        {
            shootTarget = null;
            fireRate = _cooldown;
        }

        



    }

    private void OnTriggerEnter(Collider other)
    {
        if(((1<<other.gameObject.layer) & targetLayer) != 0)
        {
            nbTargets += 1;
            actualTargets.Add(other.transform);

        }
    }
    

    private void OnTriggerExit(Collider other)
    {
        if(((1<<other.gameObject.layer) & targetLayer) != 0)
        {
            nbTargets -= 1;
            actualTargets.Remove(other.transform);
        }
    }


    public void Shoot()
    {
        Instantiate(fire, firepoint.position,firepoint.rotation);
    }

    public void TakeDamage(float damage)
    {
        lifePoints -= damage ;

        if (lifePoints <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        ravaged = true;
        Destroy(gameObject);
    }
}
