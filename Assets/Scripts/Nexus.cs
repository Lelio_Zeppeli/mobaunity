﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Nexus : MonoBehaviour
{
    public int health;
    public int damage;
    public GameObject SliderUI;
    public Slider healthBar;
    public bool turretup;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame

    void Update()
    {

        
            if (health <= 0)
        {
            Die();
        }
        healthBar.value = health;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (GameObject.FindGameObjectWithTag("Bullet"))
        {
            Damage();
        }
    }

    public void Damage()
    {
        if (GameObject.Find("Tourelle").GetComponent<TourelleSphere>().ravaged == true)

        {
            SliderUI.SetActive(true);
            health -= damage;
            if (health == 0)
                Die();
            healthBar.value = health;
        }
        
    }


    void Die()
    {
        Time.timeScale = 0f;
        //Victory
    }

    

}
