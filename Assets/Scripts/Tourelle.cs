﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tourelle : MonoBehaviour
{
    public float rayDistance;
    public LayerMask layers;
    public float angle;
    public uint nbRaycast;

    public Transform detection;
    public Transform firePoint;
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
      
        if (Physics.Raycast(detection.position, Quaternion.AngleAxis(angle, Vector3.up) * Vector3.forward * rayDistance,out hit,layers))
        {
            Debug.Log(hit.transform.gameObject.name); 
            Debug.DrawLine(firePoint.position,hit.transform.gameObject.transform.position);
        }
        
        Debug.DrawRay(detection.position, Quaternion.AngleAxis(angle, Vector3.up) * Vector3.forward * rayDistance, Color.red);
        
        for (int i = 0; i < nbRaycast; i++)
        {
            if (Physics.Raycast(detection.position,
                Quaternion.AngleAxis(angle / nbRaycast * i, Vector3.up) * Vector3.forward * rayDistance,out hit,layers))
            {
                Debug.Log(hit.transform.gameObject.name); 
                Debug.DrawLine(firePoint.position,hit.transform.gameObject.transform.position);
            }
            Debug.DrawRay(detection.position, Quaternion.AngleAxis(angle/nbRaycast*i, Vector3.up) * Vector3.forward * rayDistance, Color.red);
        }
        
    }
}
